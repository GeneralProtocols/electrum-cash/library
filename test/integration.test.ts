// Load the testing framework.
import { expect, test } from 'vitest';
import { UseCase, useCases } from './use-cases';
import { ElectrumClient } from '../source';
import { ElectrumTcpSocket } from '@electrum-cash/tcp-socket';
import { ElectrumWebSocket } from '@electrum-cash/web-socket';

// Declare use case as a global-scope reference variable.
let useCase: UseCase;

// Set up client request test
const testEncryptedClientRequest = async function(): Promise<void>
{
	// Configure an unencrypted socket.
	const encryptedTcpSocket = new ElectrumTcpSocket('bch.imaginary.cash', 50002, true);

	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', encryptedTcpSocket);

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up unencrypted client request test
const testUnencryptedClientRequest = async function(): Promise<void>
{
	// Configure an unencrypted socket.
	const unencryptedTcpSocket = new ElectrumTcpSocket('bch.imaginary.cash', 50001, false);

	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', unencryptedTcpSocket);

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up client request test using WebSockets
const testEncryptedWebSocketClientRequest = async function(): Promise<void>
{
	// Configure a secured web socket.
	const encryptedWebSocket = new ElectrumWebSocket('bch.imaginary.cash', 50004, true);

	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', encryptedWebSocket);

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up unencrypted client request test using WebSockets
const testUnencryptedWebSocketClientRequest = async function(): Promise<void>
{
	// Configure an unsecured web socket.
	const unencryptedWebSocket = new ElectrumWebSocket('bch.imaginary.cash', 50003, false);

	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', unencryptedWebSocket);

	// Wait for the client to connect
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testRestartedClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Wait for the client to connect.
	await electrum.connect();

	// Close the connection.
	await electrum.disconnect();

	// Reconnect the client.
	await electrum.connect();

	// Perform the request according to the use case.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testRequestAbortOnConnectionLossClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Wait for the client to connect.
	await electrum.connect();

	// Perform the request according to the use case (but do not await it).
	// @ts-ignore
	const requestPromise = electrum.request(...useCase.request.input);

	// Immediately close the underlying connection.
	// @ts-ignore
	await electrum.connection.disconnect();

	// Check that the request promise resolves with an Error indicating lost connection.
	expect(await requestPromise instanceof Error).toBeTruthy();
	expect((await requestPromise as Error).message).toEqual('Connection lost');
};

const testConnectOnAlreadyConnectedClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Invoke connect the first time and wait for it to be connected.
	await electrum.connect();

	// Attempt to invoke it a second time despite being already connected.
	// NOTE: It should return immediately as it is already connected and should not throw any errors in subsequent calls.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testSimultaneousConnectsOnClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Invoke multiple connects at once but DO NOT await them as want to test that this call can be made simultaneously.
	electrum.connect();
	electrum.connect();
	electrum.connect();

	// Now finally make one more connect call but this time await it.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testSimultaneousDisconnectsOnClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Invoke connect the first time and wait for it to be connected.
	await electrum.connect();

	// Invoke multiple disconnects at once but DO NOT await them as want to test that this call can be made simultaneously.
	electrum.disconnect();
	electrum.disconnect();
	electrum.disconnect();

	// Now finally make one more disconnect call but this time await it.
	await electrum.disconnect();

	// Connect again so that we can test our sample test-case.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

const testSimultaneousDisconnectAndConnectOnClient = async function(): Promise<void>
{
	// Initialize an electrum client.
	const electrum = new ElectrumClient('Electrum client test', '1.4.1', 'bch.imaginary.cash');

	// Connect to our Electrum Client and await it to make sure it is successful.
	await electrum.connect();

	// Invoke a disconnect but do not await it so that we can make it race with the below connect() call.
	electrum.disconnect();

	// Connect to our Electrum Client.
	await electrum.connect();

	// Perform a request according to the use case to ensure everything works as expected.
	// @ts-ignore
	const requestOutput = await electrum.request(...useCase.request.input);

	// Close the connection synchronously.
	await electrum.disconnect();

	// Verify that the transaction hex matches expectations.
	expect(requestOutput).toEqual(useCase.request.output);
};

// Set up normal tests.
const runNormalTests = async function(): Promise<void>
{
	// For each use case to test..
	for(const currentUseCase in useCases)
	{
		// .. assign it to the use case global reference.
		useCase = useCases[currentUseCase];

		test('Request data from encrypted TCP client', testEncryptedClientRequest);
		test('Request data from unencrypted TCP client', testUnencryptedClientRequest);
		test('Request data from client using WebSocket connection', testEncryptedWebSocketClientRequest);
		test('Request data from unencrypted client using WebSocket connection', testUnencryptedWebSocketClientRequest);
		test('Request data after restarting client', testRestartedClient);
		test('Abort active request on connection loss for a client', testRequestAbortOnConnectionLossClient);
		test('Test connect() on already connected client', testConnectOnAlreadyConnectedClient);
		test('Test simultaneous connect() calls on client', testSimultaneousConnectsOnClient);
		test('Test simultaneous disconnect() calls on client', testSimultaneousDisconnectsOnClient);
		test('Test simultaneous disconnect() and connect() calls on client', testSimultaneousDisconnectAndConnectOnClient);
	}
};

const runTests = async function(): Promise<void>
{
	// Run normal tests.
	await runNormalTests();
};

// Run all tests.
runTests();
