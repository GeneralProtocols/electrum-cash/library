import debug from '@electrum-cash/debug-logs';
import { ElectrumWebSocket } from '@electrum-cash/web-socket';
import { ElectrumProtocol } from './electrum-protocol';
import { ElectrumNetworkOptions, ElectrumConnectionEvents, ElectrumSocket, ResolveFunction, RejectFunction, VersionNegotiationResponse, isVersionRejected } from './interfaces';
import { isRPCNotification, isRPCErrorResponse, RPCResponse, RPCErrorResponse } from './rpc-interfaces';
import { EventEmitter } from 'eventemitter3';
import { ConnectionStatus } from './enums';
import { parse, parseNumberAndBigInt } from 'lossless-json';

/**
 * Wrapper around TLS/WSS sockets that gracefully separates a network stream into Electrum protocol messages.
 */
export class ElectrumConnection extends EventEmitter<ElectrumConnectionEvents>
{
	// Initialize the connected flag to false to indicate that there is no connection
	public status: ConnectionStatus = ConnectionStatus.DISCONNECTED;

	// Declare empty timestamps
	private lastReceivedTimestamp: number;

	// Declare an empty socket.
	private socket: ElectrumSocket;

	// Declare timers for keep-alive pings and reconnection
	private keepAliveTimer?: number;
	private reconnectTimer?: number;

	// Initialize an empty array of connection verification timers.
	private verifications: Array<number> = [];

	// Initialize messageBuffer to an empty string
	private messageBuffer = '';

	/**
	 * Sets up network configuration for an Electrum client connection.
	 *
	 * @param application       - your application name, used to identify to the electrum host.
	 * @param version           - protocol version to use with the host.
	 * @param socketOrHostname  - pre-configured electrum socket or fully qualified domain name or IP number of the host
	 * @param options           - ...
	 *
	 * @throws {Error} if `version` is not a valid version string.
	 */
	constructor(
		private application: string,
		private version: string,
		private socketOrHostname: ElectrumSocket | string,
		private options: ElectrumNetworkOptions,
	)
	{
		// Initialize the event emitter.
		super();

		// Check if the provided version is a valid version number.
		if(!ElectrumProtocol.versionRegexp.test(version))
		{
			// Throw an error since the version number was not valid.
			throw(new Error(`Provided version string (${version}) is not a valid protocol version number.`));
		}

		// If a hostname was provided..
		if(typeof socketOrHostname === 'string')
		{
			// Use a web socket with default parameters.
			this.socket = new ElectrumWebSocket(socketOrHostname);
		}
		else
		{
			// Use the provided socket.
			this.socket = socketOrHostname;
		}

		// Set up handlers for connection and disconnection.
		this.socket.on('connected', this.onSocketConnect.bind(this));
		this.socket.on('disconnected', this.onSocketDisconnect.bind(this));

		// Set up handler for incoming data.
		this.socket.on('data', this.parseMessageChunk.bind(this));

		// Handle visibility changes when run in a browser environment.
		if(typeof document !== 'undefined')
		{
			document.addEventListener('visibilitychange', this.handleVisibilityChange.bind(this));
		}

		// Handle network connection changes when run in a browser environment.
		if(typeof window !== 'undefined')
		{
			window.addEventListener('online', this.handleNetworkChange.bind(this));
			window.addEventListener('offline', this.handleNetworkChange.bind(this));
		}
	}

	// Expose hostIdentifier from the socket.
	get hostIdentifier(): string
	{
		return this.socket.hostIdentifier;
	}

	// Expose port from the socket.
	get encrypted(): boolean
	{
		return this.socket.encrypted;
	}

	/**
	 * Assembles incoming data into statements and hands them off to the message parser.
	 *
	 * @param data - data to append to the current message buffer, as a string.
	 *
	 * @throws {SyntaxError} if the passed statement parts are not valid JSON.
	 */
	parseMessageChunk(data: string): void
	{
		// Update the timestamp for when we last received data.
		this.lastReceivedTimestamp = Date.now();

		// Emit a notification indicating that the connection has received data.
		this.emit('received');

		// Clear and remove all verification timers.
		this.verifications.forEach((timer) => clearTimeout(timer));
		this.verifications.length = 0;

		// Add the message to the current message buffer.
		this.messageBuffer += data;

		// Check if the new message buffer contains the statement delimiter.
		while(this.messageBuffer.includes(ElectrumProtocol.statementDelimiter))
		{
			// Split message buffer into statements.
			const statementParts = this.messageBuffer.split(ElectrumProtocol.statementDelimiter);

			// For as long as we still have statements to parse..
			while(statementParts.length > 1)
			{
				// Move the first statement to its own variable.
				const currentStatementList = String(statementParts.shift());

				// Parse the statement into an object or list of objects.
				let statementList = parse(currentStatementList, null, this.options.useBigInt ? parseNumberAndBigInt : parseFloat) as RPCResponse | RPCResponse[];

				// Wrap the statement in an array if it is not already a batched statement list.
				if(!Array.isArray(statementList))
				{
					statementList = [ statementList ];
				}

				// For as long as there is statements in the result set..
				while(statementList.length > 0)
				{
					// Move the first statement from the batch to its own variable.
					const currentStatement = statementList.shift();

					// If the current statement is a subscription notification..
					if(isRPCNotification(currentStatement))
					{
						// Emit the notification for handling higher up in the stack.
						this.emit('response', currentStatement);

						// Consider this statement handled.
						continue;
					}

					// If the current statement is a version negotiation response..
					if(currentStatement.id === 'versionNegotiation')
					{
						if(isRPCErrorResponse(currentStatement))
						{
							// Then emit a failed version negotiation response signal.
							this.emit('version', { error: currentStatement.error });
						}
						else
						{
							// Extract the software and protocol version reported.
							const [ software, protocol ] = currentStatement.result;

							// Emit a successful version negotiation response signal.
							this.emit('version', { software, protocol });
						}

						// Consider this statement handled.
						continue;
					}

					// If the current statement is a keep-alive response..
					if(currentStatement.id === 'keepAlive')
					{
						// Do nothing and consider this statement handled.
						continue;
					}

					// Emit the statements for handling higher up in the stack.
					this.emit('response', currentStatement);
				}
			}

			// Store the remaining statement as the current message buffer.
			this.messageBuffer = statementParts.shift() || '';
		}
	}

	/**
	 * Sends a keep-alive message to the host.
	 *
	 * @returns true if the ping message was fully flushed to the socket, false if
	 * part of the message is queued in the user memory
	 */
	ping(): boolean
	{
		// Write a log message.
		debug.ping(`Sending keep-alive ping to '${this.hostIdentifier}'`);

		// Craft a keep-alive message.
		const message = ElectrumProtocol.buildRequestObject('server.ping', [], 'keepAlive');

		// Send the keep-alive message.
		const status = this.send(message);

		// Return the ping status.
		return status;
	}

	/**
	 * Initiates the network connection negotiates a protocol version. Also emits the 'connect' signal if successful.
	 *
	 * @throws {Error} if the socket connection fails.
	 * @returns a promise resolving when the connection is established
	 */
	async connect(): Promise<void>
	{
		// If we are already connected return true.
		if(this.status === ConnectionStatus.CONNECTED)
		{
			return;
		}

		// Indicate that the connection is connecting
		this.status = ConnectionStatus.CONNECTING;

		// Emit a connect event now that the connection is being set up.
		this.emit('connecting');

		// Define a function to wrap connection as a promise.
		const connectionResolver = (resolve: ResolveFunction<void>, reject: RejectFunction): void =>
		{
			const rejector = (error: Error): void =>
			{
				// Set the status back to disconnected
				this.status = ConnectionStatus.DISCONNECTED;

				// Emit a connect event indicating that we failed to connect.
				this.emit('disconnected');

				// Reject with the error as reason
				reject(error);
			};

			// Replace previous error handlers to reject the promise on failure.
			this.socket.removeAllListeners('error');
			this.socket.once('error', rejector);

			// Define a function to wrap version negotiation as a callback.
			const versionNegotiator = (): void =>
			{
				// Write a log message to show that we have started version negotiation.
				debug.network(`Requesting protocol version ${this.version} with '${this.hostIdentifier}'.`);

				// remove the one-time error handler since no error was detected.
				this.socket.removeListener('error', rejector);

				// Build a version negotiation message.
				const versionMessage = ElectrumProtocol.buildRequestObject('server.version', [ this.application, this.version ], 'versionNegotiation');

				// Define a function to wrap version validation as a function.
				const versionValidator = (version: VersionNegotiationResponse): void =>
				{
					// Check if version negotiation failed.
					if(isVersionRejected(version))
					{
						// Disconnect from the host.
						this.disconnect(true);

						// Declare an error message.
						const errorMessage = 'unsupported protocol version.';

						// Log the error.
						debug.errors(`Failed to connect with ${this.hostIdentifier} due to ${errorMessage}`);

						// Reject the connection with false since version negotiation failed.
						reject(errorMessage);
					}
					// Check if the host supports our requested protocol version.
					// NOTE: the server responds with version numbers that truncate 0's, so 1.5.0 turns into 1.5.
					else if((version.protocol !== this.version) && (`${version.protocol}.0` !== this.version) && (`${version.protocol}.0.0` !== this.version))
					{
						// Disconnect from the host.
						this.disconnect(true);

						// Declare an error message.
						const errorMessage = `incompatible protocol version negotiated (${version.protocol} !== ${this.version}).`;

						// Log the error.
						debug.errors(`Failed to connect with ${this.hostIdentifier} due to ${errorMessage}`);

						// Reject the connection with false since version negotiation failed.
						reject(errorMessage);
					}
					else
					{
						// Write a log message.
						debug.network(`Negotiated protocol version ${version.protocol} with '${this.hostIdentifier}', powered by ${version.software}.`);

						// Set connection status to connected
						this.status = ConnectionStatus.CONNECTED;

						// Emit a connect event now that the connection is usable.
						this.emit('connected');

						// Resolve the connection promise since we successfully connected and negotiated protocol version.
						resolve();
					}
				};

				// Listen for version negotiation once.
				this.once('version', versionValidator);

				// Send the version negotiation message.
				this.send(versionMessage);
			};

			// Prepare the version negotiation.
			this.socket.once('connected', versionNegotiator);

			// Set up handler for network errors.
			this.socket.on('error', this.onSocketError.bind(this));

			// Connect to the server.
			this.socket.connect();
		};

		// Wait until connection is established and version negotiation succeeds.
		await new Promise<void>(connectionResolver);
	}

	/**
	 * Restores the network connection.
	 */
	async reconnect(): Promise<void>
	{
		// If a reconnect timer is set, remove it
		await this.clearReconnectTimer();

		// Write a log message.
		debug.network(`Trying to reconnect to '${this.hostIdentifier}'..`);

		// Set the status to reconnecting for more accurate log messages.
		this.status = ConnectionStatus.RECONNECTING;

		// Emit a connect event now that the connection is usable.
		this.emit('reconnecting');

		// Disconnect the underlying socket.
		this.socket.disconnect();

		try
		{
			// Try to connect again.
			await this.connect();
		}
		catch(error)
		{
			// Do nothing as the error should be handled via the disconnect and error signals.
		}
	}

	/**
	 * Removes the current reconnect timer.
	 */
	clearReconnectTimer(): void
	{
		// If a reconnect timer is set, remove it
		if(this.reconnectTimer)
		{
			clearTimeout(this.reconnectTimer);
		}

		// Reset the timer reference.
		this.reconnectTimer = undefined;
	}

	/**
	 * Removes the current keep-alive timer.
	 */
	clearKeepAliveTimer(): void
	{
		// If a keep-alive timer is set, remove it
		if(this.keepAliveTimer)
		{
			clearTimeout(this.keepAliveTimer);
		}

		// Reset the timer reference.
		this.keepAliveTimer = undefined;
	}

	/**
	 * Initializes the keep alive timer loop.
	 */
	setupKeepAliveTimer(): void
	{
		// If the keep-alive timer loop is not currently set up..
		if(!this.keepAliveTimer)
		{
			// Set a new keep-alive timer.
			this.keepAliveTimer = setTimeout(this.ping.bind(this), this.options.sendKeepAliveIntervalInMilliSeconds) as unknown as number;
		}
	}

	/**
	 * Tears down the current connection and removes all event listeners on disconnect.
	 *
	 * @param force       - disconnect even if the connection has not been fully established yet.
	 * @param intentional - update connection state if disconnect is intentional.
	 *
	 * @returns true if successfully disconnected, or false if there was no connection.
	 */
	async disconnect(force: boolean = false, intentional: boolean = true): Promise<boolean>
	{
		// Return early when there is nothing to disconnect from
		if(this.status === ConnectionStatus.DISCONNECTED && !force)
		{
			// Return false to indicate that there was nothing to disconnect from.
			return false;
		}

		// Update connection state if the disconnection is intentional.
		// NOTE: The state is meant to represent what the client is requesting, but
		//       is used internally to handle visibility changes in browsers to ensure functional reconnection.
		if(intentional)
		{
			// Set connection status to null to indicate tear-down is currently happening.
			this.status = ConnectionStatus.DISCONNECTING;
		}

		// Emit a connect event to indicate that we are disconnecting.
		this.emit('disconnecting');

		// If a keep-alive timer is set, remove it.
		await this.clearKeepAliveTimer();

		// If a reconnect timer is set, remove it
		await this.clearReconnectTimer();

		const disconnectResolver = (resolve: ResolveFunction<boolean>): void =>
		{
			// Resolve to true after the connection emits a disconnect
			this.once('disconnected', () => resolve(true));

			// Close the connection on the socket level.
			this.socket.disconnect();
		};

		// Return true to indicate that we disconnected.
		return new Promise<boolean>(disconnectResolver);
	}

	/**
	 * Updates the connection state based on browser reported connectivity.
	 *
	 * Most modern browsers are able to provide information on the connection state
	 * which allows for significantly faster response times to network changes compared
	 * to waiting for network requests to fail.
	 *
	 * When available, we make use of this to fail early to provide a better user experience.
	 */
	async handleNetworkChange(): Promise<void>
	{
		// Do nothing if we do not have the navigator available.
		if(typeof window.navigator === 'undefined')
		{
			return;
		}

		// Attempt to reconnect to the network now that we may be online again.
		if(window.navigator.onLine === true)
		{
			this.reconnect();
		}

		// Disconnected from the network so that cleanup can happen while we're offline.
		if(window.navigator.onLine !== true)
		{
			const forceDisconnect = true;
			const isUnintended = false;

			this.disconnect(forceDisconnect, isUnintended);
		}
	}

	/**
	 * Updates connection state based on application visibility.
	 *
	 * Some browsers will disconnect network connections when the browser is out of focus,
	 * which would normally cause our reconnect-on-timeout routines to trigger, but that
	 * results in a poor user experience since the events are not handled consistently
	 * and sometimes it can take some time after restoring focus to the browser.
	 *
	 * By manually disconnecting when this happens we prevent the default reconnection routines
	 * and make the behavior consistent across browsers.
	 */
	async handleVisibilityChange(): Promise<void>
	{
		// Disconnect when application is removed from focus.
		if(document.visibilityState === 'hidden')
		{
			const forceDisconnect = true;
			const isUnintended = false;

			this.disconnect(forceDisconnect, isUnintended);
		}

		// Reconnect when application is returned to focus.
		if(document.visibilityState === 'visible')
		{
			this.reconnect();
		}
	}

	/**
	 * Sends an arbitrary message to the server.
	 *
	 * @param message - json encoded request object to send to the server, as a string.
	 *
	 * @returns true if the message was fully flushed to the socket, false if part of the message
	 * is queued in the user memory
	 */
	send(message: string): boolean
	{
		// Remove the current keep-alive timer if it exists.
		this.clearKeepAliveTimer();

		// Get the current timestamp in milliseconds.
		const currentTime = Date.now();

		// Follow up and verify that the message got sent..
		const verificationTimer = setTimeout(this.verifySend.bind(this, currentTime), this.socket.timeout) as unknown as number;

		// Store the verification timer locally so that it can be cleared when data has been received.
		this.verifications.push(verificationTimer);

		// Set a new keep-alive timer.
		this.setupKeepAliveTimer();

		// Write the message to the network socket.
		return this.socket.write(message + ElectrumProtocol.statementDelimiter);
	}

	// --- Event managers. --- //

	/**
	 * Marks the connection as timed out and schedules reconnection if we have not
	 * received data within the expected time frame.
	 */
	verifySend(sentTimestamp: number): void
	{
		// If we haven't received any data since we last sent data out..
		if(Number(this.lastReceivedTimestamp) < sentTimestamp)
		{
			// If this connection is already disconnected, we do not change anything
			if((this.status === ConnectionStatus.DISCONNECTED) || (this.status === ConnectionStatus.DISCONNECTING))
			{
				// debug.warning(`Tried to verify already disconnected connection to '${this.hostIdentifier}'`);

				return;
			}

			// Remove the current keep-alive timer if it exists.
			this.clearKeepAliveTimer();

			// Write a notification to the logs.
			debug.network(`Connection to '${this.hostIdentifier}' timed out.`);

			// Close the connection to avoid re-use.
			// NOTE: This initiates reconnection routines if the connection has not
			//       been marked as intentionally disconnected.
			this.socket.disconnect();
		}
	}

	/**
	 * Updates the connection status when a connection is confirmed.
	 */
	onSocketConnect(): void
	{
		// If a reconnect timer is set, remove it.
		this.clearReconnectTimer();

		// Set up the initial timestamp for when we last received data from the server.
		this.lastReceivedTimestamp = Date.now();

		// Set up the initial keep-alive timer.
		this.setupKeepAliveTimer();

		// Clear all temporary error listeners.
		this.socket.removeAllListeners('error');

		// Set up handler for network errors.
		this.socket.on('error', this.onSocketError.bind(this));
	}

	/**
	 * Updates the connection status when a connection is ended.
	 */
	onSocketDisconnect(): void
	{
		// Remove the current keep-alive timer if it exists.
		this.clearKeepAliveTimer();

		// If this is a connection we're trying to tear down..
		if(this.status === ConnectionStatus.DISCONNECTING)
		{
			// Mark the connection as disconnected.
			this.status = ConnectionStatus.DISCONNECTED;

			// Send a disconnect signal higher up the stack.
			this.emit('disconnected');

			// If a reconnect timer is set, remove it.
			this.clearReconnectTimer();

			// Remove all event listeners
			this.removeAllListeners();

			// Write a log message.
			debug.network(`Disconnected from '${this.hostIdentifier}'.`);
		}
		else
		{
			// If this is for an established connection..
			if(this.status === ConnectionStatus.CONNECTED)
			{
				// Write a notification to the logs.
				debug.errors(`Connection with '${this.hostIdentifier}' was closed, trying to reconnect in ${this.options.reconnectAfterMilliSeconds / 1000} seconds.`);
			}
			// If this is a connection that is currently connecting, reconnecting or already disconnected..
			else
			{
				// Do nothing

				// NOTE: This error message is useful during manual debugging of reconnections.
				// debug.errors(`Lost connection with reconnecting or already disconnected server '${this.hostIdentifier}'.`);
			}

			// Mark the connection as disconnected for now..
			this.status = ConnectionStatus.DISCONNECTED;

			// Send a disconnect signal higher up the stack.
			this.emit('disconnected');

			// If we don't have a pending reconnection timer..
			if(!this.reconnectTimer)
			{
				// Attempt to reconnect after one keep-alive duration.
				this.reconnectTimer = setTimeout(this.reconnect.bind(this), this.options.reconnectAfterMilliSeconds) as unknown as number;
			}
		}
	}

	/**
	 * Notify administrator of any unexpected errors.
	 */
	onSocketError(error: any | undefined): void
	{
		// Report a generic error if no error information is present.
		// NOTE: When using WSS, the error event explicitly
		//       only allows to send a "simple" event without data.
		//       https://stackoverflow.com/a/18804298
		if(typeof error === 'undefined')
		{
			// Do nothing, and instead rely on the socket disconnect event for further information.
			return;
		}

		// Log the error, as there is nothing we can do to actually handle it.
		debug.errors(`Network error ('${this.hostIdentifier}'): `, error);
	}
}
