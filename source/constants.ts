import { ElectrumNetworkOptions } from './interfaces';

// Define number of milliseconds per second for legibility.
const MILLI_SECONDS_PER_SECOND = 1000;

/**
 * Configure default options.
 */
export const defaultNetworkOptions: ElectrumNetworkOptions =
{
	// By default, all numbers including integers are parsed as regular JavaScript numbers.
	useBigInt: false,

	// Send a ping message every seconds, to detect network problem as early as possible.
	sendKeepAliveIntervalInMilliSeconds: 1 * MILLI_SECONDS_PER_SECOND,

	// Try to reconnect 5 seconds after unintentional disconnects.
	reconnectAfterMilliSeconds: 5 * MILLI_SECONDS_PER_SECOND,

	// Try to detect stale connections 5 seconds after every send.
	verifyConnectionTimeoutInMilliSeconds: 5 * MILLI_SECONDS_PER_SECOND,
};
